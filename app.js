require('./config/config');
require('./db/mongoose');

const express = require('express');
const bodyParser = require('body-parser');
const ValidationError = require('express-validation').ValidationError;
const cors = require('cors');

const authController = require('./controllers/auth.controller');
const roomController = require('./controllers/room.controller');
const bookingController = require('./controllers/booking.controller');
const responseHandler = require('./helpers/response-handler');

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.use('/api/auth', authController);
app.use('/api/rooms', roomController);
app.use('/api/booking', bookingController);

app.use((err, req, res, next) => {
  if (err instanceof ValidationError) {
    return responseHandler.handleValidationError(res, err);
  } else {
    return responseHandler.handleUnexpectedError(res, err);
  }
});

app.listen(process.env.PORT, () => {
  console.log(`HP Hotel listening on port ${process.env.PORT}`);
});
