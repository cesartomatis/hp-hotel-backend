const environment = process.env.NODE_ENV || 'dev';
// @ts-ignore
const config = require('./config.json');
const envConfig = config[environment];
Object.keys(envConfig).forEach(key => {
  process.env[key] = envConfig[key];
});
