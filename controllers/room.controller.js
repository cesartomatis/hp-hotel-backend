const router = require('express').Router();

const Room = require('../models/room');
const responseHandler = require('../helpers/response-handler');
const roomValidations = require('../middlewares/validations/room.validations');

router.get('/list', async (req, res) => {
    try {
        const room = await Room.find({});
        if (!Room || Room.length === 0) {
            return responseHandler.handleBadRequestError(
                res,
                'There are no rooms.'
            );
        }
        return responseHandler.handleOKResponse(res, room);
    } catch (err) {
        return responseHandler.handleUnexpectedError(res, err);
    }
});

router.post('/add', roomValidations.room, async (req, res) => {
    try {
        const roomToAdd = new Room({
            number: req.body.number,
            capacity: req.body.capacity,
            photo: req.body.photo,
            roomType: req.body.roomType,
            description: req.body.description,
            price: req.body.price
        });
        const room = await Room.findOne({ number: roomToAdd.number });
        if (room) {
            return responseHandler.handleBadRequestError(
                res,
                'Room already exists.'
            );
        }
        await roomToAdd.save();
        return responseHandler.handleOKResponse(res, roomToAdd);
    } catch (err) {
        return responseHandler.handleUnexpectedError(res, err);
    }
});

router.get('/:number', async (req, res) => {
    try {
        const room = await Room.findOne({ number: req.params.number });
        if (!room) {
            return responseHandler.handleBadRequestError(
                res,
                "Room doesn't exists"
            );
        }
        return responseHandler.handleOKResponse(res, room);
    } catch (err) {
        return responseHandler.handleUnexpectedError(res, err);
    }
});

router.put('/:roomType', async (req, res) => {
    try {
        let rooms = await Room.find({ roomType: req.params.roomType });
        if (!rooms) {
            return responseHandler.handleBadRequestError(
                res,
                "Room doesn't exists"
            );
        }
        for (let room of rooms) {
            room.price = req.body.price;
            await Room.update(
                { number: room.number },
                {
                    price: room.price
                }
            );
        }
        return responseHandler.handleOKResponse(res, rooms);
    } catch (err) {
        return responseHandler.handleUnexpectedError(res, err);
    }
});

module.exports = router;
