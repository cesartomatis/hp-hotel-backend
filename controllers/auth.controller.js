const router = require('express').Router();

const authValidations = require('../middlewares/validations/auth.validations');
const User = require('../models/user');
const Client = require('../models/client');
const responseHandler = require('../helpers/response-handler');
const authHelper = require('../helpers/auth-helper');
const authorize = require('../middlewares/authorize');

router.post('/register', authValidations.register, async (req, res) => {
    try {
        const userToAdd = new User({
            email: req.body.email
        });
        const user = await User.findOne({ email: userToAdd.email });
        if (user) {
            return responseHandler.handleBadRequestError(
                res,
                'User is already registered'
            );
        }
        userToAdd.password = authHelper.hashPassword(req.body.password);
        await userToAdd.save();
        return responseHandler.handleOKResponse(res, {
            email: userToAdd.email,
            firstName: userToAdd.firstName,
            lastName: userToAdd.lastName
        });
    } catch (err) {
        return responseHandler.handleUnexpectedError(res, err);
    }
});

router.get('/checkSession', authorize, async (req, res) => {
    try {
        // @ts-ignore
        const user = await User.findOne({ _id: req.userId });
        const token = req.headers['authorization'];
        if (!user) {
            return responseHandler.handleBadRequestError(res, {
                message: 'User is not logged in'
            });
        }
        return responseHandler.handleOKResponse(res, {
            email: user.email,
            token
        });
    } catch (err) {
        return responseHandler.handleUnexpectedError(res, err);
    }
});

router.post('/login', authValidations.login, async (req, res) => {
    try {
        const user = await User.findOne({ email: req.body.email });
        let passwordValid = false;
        if (user) {
            passwordValid = authHelper.isPasswordValid(
                req.body.password,
                user.password
            );
        }
        if (!passwordValid) {
            return responseHandler.handleBadRequestError(
                res,
                'Incorrect user or password'
            );
        }
        const token = authHelper.createAuthToken(user._id);
        return responseHandler.handleOKResponse(res, {
            email: req.body.email,
            token
        });
    } catch (err) {
        return responseHandler.handleUnexpectedError(res, err);
    }
});

router.post(
    '/client/register',
    authValidations.registerClient,
    async (req, res) => {
        try {
            const clientToAdd = new Client({
                email: req.body.email,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                dni: req.body.dni,
                birthDate: req.body.birthDate,
                phone: req.body.phone,
                address: req.body.address
            });
            const client = await Client.findOne({ dni: clientToAdd.dni });
            if (client) {
                return responseHandler.handleBadRequestError(
                    res,
                    'Client is already registered'
                );
            }
            await clientToAdd.save();
            return responseHandler.handleOKResponse(res, {
                dni: clientToAdd.dni,
                firstName: clientToAdd.firstName,
                lastName: clientToAdd.lastName
            });
        } catch (err) {
            return responseHandler.handleUnexpectedError(res, err);
        }
    }
);

router.get('/client/:dni', async (req, res) => {
    try {
        const client = await Client.findOne({ dni: req.params.dni });
        if (!client) {
            return responseHandler.handleBadRequestError(
                res,
                'Client does not exists'
            );
        }
        return responseHandler.handleOKResponse(res, client);
    } catch (err) {
        return responseHandler.handleUnexpectedError(res, err);
    }
});

module.exports = router;
