const router = require('express').Router();

const Booking = require('../models/booking');
const bookingValidations = require('../middlewares/validations/booking.validations');
const responseHandler = require('../helpers/response-handler');

router.post('/add', bookingValidations.addBooking, async (req, res) => {
  try {
    const bookingToAdd = new Booking({
      clientId: req.body.clientId,
      roomId: req.body.roomId,
      checkIn: '',
      checkOut: '',
      fromDate: req.body.fromDate,
      toDate: req.body.toDate,
      guests: undefined,
      services: undefined
    });
    await bookingToAdd.save();
    return responseHandler.handleOKResponse(res, {
      message: 'Booking added successfully.'
    });
  } catch (err) {
    return responseHandler.handleUnexpectedError(res, err);
  }
});

router.get('/list', async (req, res) => {
  try {
    const bookingList = await Booking.find({});
    if (!bookingList || bookingList.length === 0) {
      return responseHandler.handleBadRequestError(res, {
        message: 'There are no bookings.'
      });
    }
    return responseHandler.handleOKResponse(res, bookingList);
  } catch (err) {
    return responseHandler.handleUnexpectedError(res, err);
  }
});

router.put('/update/:id', bookingValidations.addBooking, async (req, res) => {
  try {
    const bookingToUpdate = {
      clientId: req.body.clientId,
      roomId: req.body.roomId,
      checkIn: req.body.checkIn,
      checkOut: req.body.checkOut,
      fromDate: req.body.fromDate,
      toDate: req.body.toDate,
      guests: req.body.guests,
      services: req.body.services
    };
    await Booking.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: bookingToUpdate
      }
    );
    return responseHandler.handleOKResponse(res, {
      message: 'Booking updated successfully.'
    });
  } catch (err) {
    return responseHandler.handleUnexpectedError(res, err);
  }
});

router.delete('/remove/:id', async (req, res) => {
  Booking.findByIdAndDelete({ _id: req.params.id }, (err, booking) => {
    if (err) {
      return responseHandler.handleBadRequestError(res, err);
    }
    const response = {
      message: 'Booking successfully deleted',
      id: req.params.id
    };
    return responseHandler.handleOKResponse(res, response);
  });
});

module.exports = router;
