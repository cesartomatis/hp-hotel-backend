const mongoose = require('mongoose');

const Booking = mongoose.model(
    'booking',
    new mongoose.Schema({
        clientId: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        roomId: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        checkIn: {
            type: String,
            required: false,
            minlength: 0,
            trim: true,
            unique: false
        },
        checkOut: {
            type: String,
            required: false,
            minlength: 0,
            trim: true,
            unique: false
        },
        fromDate: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        toDate: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        guests: [
            {
                name: {
                    type: String,
                    required: true,
                    minlength: 1,
                    trim: true,
                    unique: false
                },
                lastname: {
                    type: String,
                    required: true,
                    minlength: 1,
                    trim: true,
                    unique: false
                },
                dni: {
                    type: String,
                    required: true,
                    minlength: 1,
                    trim: true,
                    unique: false
                }
            }
        ],
        services: [
            {
                name: {
                    type: String,
                    required: false,
                    minlength: 1,
                    trim: true,
                    unique: false
                },
                price: {
                    type: Number,
                    required: false,
                    unique: false
                },
                date: {
                    type: String,
                    required: false,
                    minlength: 1,
                    trim: true,
                    unique: false
                }
            }
        ]
    })
);

module.exports = Booking;
