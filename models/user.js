const mongoose = require('mongoose');

const User = mongoose.model(
    'users',
    new mongoose.Schema({
        email: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: true
        },
        password: {
            type: String,
            minlength: 6,
            required: true
        }
    })
);

module.exports = User;
