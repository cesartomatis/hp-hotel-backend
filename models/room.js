const mongoose = require('mongoose');

const Room = mongoose.model(
    'rooms',
    new mongoose.Schema({
        number: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: true
        },
        capacity: {
            type: Number,
            required: true,
            unique: false
        },
        photo: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        roomType: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        description: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        price:{
            type: Number,
            required: true,
            unique: false
        }
    })
);

module.exports = Room;
