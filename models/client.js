const mongoose = require('mongoose');

const Client = mongoose.model(
    'client',
    new mongoose.Schema({
        email: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: true
        },
        dni: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: true
        },
        firstName: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        lastName: {
            type: String,
            required: false,
            trim: true,
            unique: false
        },
        birthDate: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        phone: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        },
        address: {
            type: String,
            required: true,
            minlength: 1,
            trim: true,
            unique: false
        }
    })
);

module.exports = Client;
