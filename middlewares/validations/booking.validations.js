const validate = require('express-validation');
const joi = require('joi');

module.exports = {
    addBooking: validate({
        body: {
            clientId: joi
                .string()
                .min(1)
                .required(),
            roomId: joi
                .string()
                .min(1)
                .required(),
            checkIn: joi
                .string()
                .allow('')
                .optional(),
            checkOut: joi
                .string()
                .allow('')
                .optional(),
            fromDate: joi
                .string()
                .min(1)
                .required(),
            toDate: joi
                .string()
                .min(1)
                .required(),
            guests: joi.array().optional(),
            services: joi.array().optional()
        }
    })
};
