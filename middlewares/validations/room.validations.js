const validate = require('express-validation');
const joi = require('joi');

module.exports = {
    room: validate({
        body: {
            number: joi
                .string()
                .min(1)
                .required(),
            capacity: joi.number().required(),
            photo: joi
                .string()
                .min(1)
                .required(),
            roomType: joi
                .string()
                .min(1)
                .required(),
            description: joi
                .string()
                .min(1)
                .required(),
            price: joi.number().required()
        }
    })
};
