const authHelper = require('../helpers/auth-helper');
const responseHandler = require('../helpers/response-handler');
const HTTP_STATUS = require('http-status-codes');

const authorize = (req, res, next) => {
    const token = req.headers['authorization'];
    if (!token) {
        return responseHandler.handleCustomErrorResponse(
            res,
            HTTP_STATUS.UNAUTHORIZED
        );
    }
    const decodedToken = authHelper.decodeToken(token);
    // @ts-ignore
    if (!decodedToken) {
        return responseHandler.handleCustomErrorResponse(
            res,
            HTTP_STATUS.UNAUTHORIZED
        );
    }
    // @ts-ignore
    req.userId = decodedToken.id;
    next();
};

module.exports = authorize;
